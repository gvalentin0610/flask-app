FROM python:3.7-alpine
WORKDIR /app
RUN apk add --no-cache gcc musl-dev linux-headers postgresql-libs postgresql-dev
COPY . /app
RUN pip install -r requirements.txt
EXPOSE 5000
CMD ["python", "take_home.py"]