'''
This application won't work! Your challenge is to fix that.

Please connect this application to a persistent database and encapsulate it in
such a way that you can send us a zip of the dir and we can have this up and
running if we have the following dependencies installed:

* Docker
* Python
* Flask
* SqlAlchemy

How you do so is entirely up to you.

Deliverable: a zip or otherwise compressed file. This file MUST include
take_home.py and it MUST include a README.md that will explain how to run this
app so it works.

HINT: The less we need to type to see that it works, the better :)
'''
import os
from flask import Flask
from time import sleep
from flask_sqlalchemy import SQLAlchemy
import psycopg2


app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = f'postgresql+psycopg2://gus:pitufresas@database:5432/akitest'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80), unique=False, nullable=False)
    email = db.Column(db.String(120), unique=False, nullable=False)

    def __repr__(self):
        return '<User %r>' % self.username


@app.route("/")
def hello():
    admin = User(username='admin', email='admin@example.com')
    db.session.add(admin)
    db.session.commit()
    return "It works"


sleep(60)
db.create_all()
app.run(host='0.0.0.0')
